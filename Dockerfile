FROM alpine
ADD bin/main /bin/main
RUN apk -Uuv add ca-certificates
ENTRYPOINT /bin/main
