package model

type App struct {
	BaseUrl     string   `env:"PLUGIN_URL" envDefault:"https://ntfy.sh"`
	Topic       string   `env:"PLUGIN_TOPIC,required"`
	Username    string   `env:"PLUGIN_USERNAME"`
	Password    string   `env:"PLUGIN_PASSWORD"`
	Token       string   `env:"PLUGIN_TOKEN"`
	Title       string   `env:"PLUGIN_TITLE"`
	Priority    string   `env:"PLUGIN_PRIORITY" envDefault:"default"`
	Tags        []string `env:"PLUGIN_TAGS" envSeparator:","`
	DefaultTags []string `env:"PLUGIN_DEFAULT_TAGS" envDefault:"drone"`
	Message     string   `env:"PLUGIN_MESSAGE"`
}

type Drone struct {
	BuildNumber string `env:"DRONE_BUILD_NUMBER"`
	BuildStatus string `env:"DRONE_BUILD_STATUS"`
	Tag         string `env:"DRONE_TAG"`
	BuildLink   string `env:"DRONE_BUILD_LINK"`
	CommitLink  string `env:"DRONE_COMMIT_LINK"`
	RepoName    string `env:"DRONE_REPO_NAME"`
}

type Ci struct {
	CommitMessage string `env:"CI_COMMIT_MESSAGE"`
	CommitSha     string `env:"CI_COMMIT_SHA"`
	CommitBranch  string `env:"CI_COMMIT_BRANCH"`
	CommitRef     string `env:"CI_COMMIT_REF"`
}
