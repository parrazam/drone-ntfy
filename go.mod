module git.parravidales.es/parra/drone-ntfy

go 1.19

require (
	github.com/caarlos0/env/v7 v7.0.0
	github.com/joho/godotenv v1.5.1
)
