# Drone Ntfy plugin

[![Build Status](https://ci.cuzo.dev/api/badges/parra/drone-ntfy/status.svg)](https://ci.cuzo.dev/parra/drone-ntfy)

The Ntfy.sh plugin posts build status messages to your selected Ntfy server.

Example:
```yml
kind: pipeline
name: default

steps:
- name: send ntfy notification
  image: parrazam/drone-ntfy
  when:
      status: [success, failure]
  settings:
    url: https://ntfy.example.org
    topic: events
    priority: low
    tags:
      - pipeline-status
      - dev
    token:
      from_secret: ntfy_token
```

## Properties

`url` *string* [optional] \
Ntfy server.
> *Default: https://ntfy.sh*

`topic` *string* [**REQUIRED**] \
Topic to publish message.
> *Default: none*

`priority` *string* [optional] \
Priority of the notification. Values can be [min, low, default, high, max].
> *Default: default*

`tags` *string* [optional] \
Custom tags to include.
> *Default: none*

`username` *string* [optional] \
Username with publish permissions.
> *Default: none*

`password` *string* [optional] \
[***SECRET RECOMMENDED***] \
Password for username.

> *Default: none*

`token` *string* [optional] \
[***SECRET RECOMMENDED***] \
Token to use, instead username and password.

> *Default: none*

## Development

If you want to test the project locally, first you need to create an `.env` file with:

```env
PLUGIN_TOPIC=
PLUGIN_URL=
PLUGIN_USERNAME=
PLUGIN_PASSWORD=
PLUGIN_TOKEN=
CI_COMMIT_SHA=
CI_COMMIT_MESSAGE=
CI_REPO_NAME=
CI_COMMIT_BRANCH=
CI_COMMIT_REF=
DRONE_BUILD_NUMBER=
DRONE_BUILD_STATUS=
DRONE_STAGE_NUMBER=
DRONE_STAGE_STATUS=
DRONE_TAG=
DRONE_BUILD_LINK=
DRONE_COMMIT_LINK=
DRONE_REPO_NAME=
```
